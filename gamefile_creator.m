close all;
clear all;
clc;

%===== Read master_player_list.txt =====%
%master_user_list_dir = 'Z:\cfg\SpotGameCfg\test_player_list.txt';
master_user_list_dir = 'S:\SpotGameCfg\master_player_list.txt'; 
fileID = fopen(master_user_list_dir,'r');
list_cell = textscan(fileID,'%s');
master_user_list = list_cell{1,1};          % N x 1 cell of player names
[user_cnt,~] = size(master_user_list); 
fclose(fileID); 

%===== Gamefile Parameters =====%
gamefile_dst_dir = 'S:\SpotGameCfg\tmp_user_gamefiles'; 
defect_type = 'percentDebris'; 
defect_thresh_hi = 0.3;
defect_thresh_low = 0.05;  
defect_annotate = 'percentDebris';
defect_ui_label = 'Debris'; 

num_spots = 425;
num_coverage = 200;
num_democracy = 200;
num_benchmark = 5;  %Actual benchmark spot = num_benchmark * (benchmark_repeat+1) 
%{
num_spots = 250;
num_coverage = 195;
num_democracy = 40;
num_benchmark = 5;
%}

defect_percentage = 0.5;
num_coverage_defect = floor(num_coverage * defect_percentage);
num_democracy_defect = floor(num_democracy * defect_percentage);

% Benchmark spots revisited an extra N times 
% (EX. 3 repeats of a spot, benchmark repeat = 2, because we copy existing 
% spot 2 times)
benchmark_repeat = 4;           
num_actual_spots = num_coverage + num_democracy + ... 
            (num_benchmark * (benchmark_repeat+1)); 

if( num_spots ~= num_actual_spots )
    errordlg('Total number of coverage:democracy:benchmark spots is not equal to desired number of spots', 'Parameter Error!'); 
    return
end 

master_spot_cell = {}; 

%{
batch_num_list = { 220099};
root_path_list = { 'M:/Chip Data/220099/Lowmag'};              
spot_img_path_list = {  'M:/Chip Data/220099/Lowmag/report - 26-Jan-2022 14.44.03/Spot_Images'};
%}


batch_num_list = {  220069; ...
                    220044;  ...
                    220071; ...
                    220070};
                
root_path_list = { 'M:/Chip Data/220069/Lowmag'; ...
                   'M:/Chip Data/220044/Lowmag';...
                   'M:/Chip Data/220071/Low Mag'; ...
                   'M:/Chip Data/220070/Low Mag'}; 
               
spot_img_path_list = {  'M:/Chip Data/220069/Lowmag/report - 20-Jan-2022 13.38.13/Spot_Images'; ...
                        'M:/Chip Data/220044/Lowmag/report - 13-Jan-2022 12.48.41/Spot_Images'; ...
                        'M:/Chip Data/220071/Low Mag/report - 20-Jan-2022 13.21.18/Spot_Images'; ...
                        'M:/Chip Data/220070/Low Mag/report - 20-Jan-2022 12.43.04/Spot_Images'};

[batch_cnt, ~] = size(batch_num_list); 

for xx = 1:batch_cnt
    %===== Spot .xml source file Parsing =====%
    batch_num = batch_num_list{xx}; 
    root_path = root_path_list{xx}; 
    spot_img_path = spot_img_path_list{xx};
    spot_img_content = dir(spot_img_path); 
    spot_img_list = {spot_img_content.name};

    xml_path = fullfile(root_path, [num2str(batch_num) '0' '.xml']);

    fprintf(['BEGIN XML PARSING ' num2str(batch_num) '\n']); 
    xml_struct = FILE_xml2struct(xml_path);
    fprintf(['XML PARSING ' num2str(batch_num) 'COMPLETE \n']); 

    chip_list = xml_struct.batchfile.iris; 

    % Get spotinfo fields 
    spotinfo_fieldnames = fieldnames(chip_list{1,1}.spot{1,1}.spotinfo); 

    %----- Construct CSV file -----%
    % Make a copy of 'gamefile_template.csv'
    %copyfile('gamefile_template.csv', 'test_gamefile.csv');

    %fprintf('CONSTRUCTING CSV FILE \n'); 
    %[csv_cell,s] = xlsread('test_gamefile.csv'); 

    % Create cell array (transform into .csv table later) 
    csv_cell = {[]};
    for ii  = 1:length(spotinfo_fieldnames)
        csv_cell{1, 1 + ii} = spotinfo_fieldnames{ii,1};
    end

    csv_cell{1, end + 1} = 'img_path';
    csv_cell{1, end + 1} = 'user_decision'; 


    %csv_cell = {[] 'spotnum' 'QC_version' 'fittedname' 'img_name' ...
    %            'xnum' 'ynum' 'xpixel' 'ypixel' 'CV' 'circularity'... 
    %            'user_decision'}; 

    spot_ind = 2;
    col_cnt = 1; 
    for ii = 1:length(chip_list)
        chip_param = chip_list{1,ii}.dataparameters;
        qc_version = chip_param.QCversion.Text; 
        fit_name = chip_param.fittedfilename.Text;
        int_chip_name = chip_param.internalchipname.Text; 

        spot_list = chip_list{1,ii}.spot;
        for jj = 1:length(spot_list)
            spotinfo = spot_list{1,jj}.spotinfo;
            field_names = fieldnames(spotinfo); 

            spot_num = str2num(spotinfo.spotnumber.Text);
            pad_num = num2str(spot_num, '%03d'); 

            spot_name = [int_chip_name '.' pad_num];

            %See if spot_name exists in spot_ing_list
            contains_arr = contains(spot_img_list, spot_name);

            if any(contains_arr)
                csv_cell{spot_ind, 1} = spot_ind - 2; 

                % Fill in row with current spot's spotinfo
                for kk = 1:numel(field_names)
                    % crop_rect data is formatted such that it will not be
                    % parsed correctly. It's not really used, so temporarily
                    % set all crop_rect values to 'NaN'; 
                    if strcmp(field_names{kk}, 'crop_rect')
                        str = spotinfo.(field_names{kk}).Text;
                        str = regexprep(str, ', ', '_');
                        csv_cell{spot_ind, kk + 1} = str; 
                    else
                        csv_cell{spot_ind, kk + 1} = ... 
                                    spotinfo.(field_names{kk}).Text; 
                    end 
                end

                % Add filename to csv_cell 
                spot_img_str = [spot_img_path '/' spot_name '.png']; 
                csv_cell{spot_ind, end - 1} = spot_img_str;

                spot_ind = spot_ind + 1; 
            end
        end
        fprintf([num2str(batch_num) ': ' num2str(ii) '/' num2str(length(chip_list)) ' CHIPS COMPLETE \n']);
    end
    
    if(xx == 1)
        master_spot_cell = csv_cell; 
    else
        tmp_spot_data = csv_cell(2:end, :);
        [spot_data_m, ~] = size(tmp_spot_data);
        
        for kk = 1:spot_data_m
            master_spot_cell(end + 1,:) = tmp_spot_data(kk, :); 
        end 
    end 
    
    
end 

%===== Add extra columns to the player gamefiles =====%

% Pre-set user_decision
master_spot_cell(2:end, end) = {-1};   % Set to -1

% Pre-set select_order
master_spot_cell{1, end + 1} = 'select_order';
master_spot_cell(2:end, end) = {0};   % Set to 0, more conducive to find(x)

% Add annotate_type col
master_spot_cell{1, end + 1} = 'annotate_type';
master_spot_cell(2:end, end) = {'X'};   % Pre-set to 'X'

gamefile_cell = master_spot_cell;
header_list = master_spot_cell(1,:); 
spot_data = master_spot_cell(2:end, :); 


user_gamefiles = {};
for ii = 1:user_cnt
    user_gamefiles{ii,1} = gamefile_cell; 
end 
 
%===== Find all spots with param 'defect_type' value =====%
%===== Greater than defect_thresh                    =====%
exact_match_mask = strcmp(header_list, defect_type);
defect_col = find(exact_match_mask);

if(defect_col == 0)
    errordlg('Defect type not found', 'Warning!'); 
    return
end 

%===== Find 'select_order' col =====%
exact_match_mask = strcmp(header_list, 'select_order');
select_order_col = find(exact_match_mask);

%===== Find 'annotate_type' col =====%
exact_match_mask = strcmp(header_list, 'annotate_type');
annotate_type_col = find(exact_match_mask);

%===== Find 'user_decision' col =====%
exact_match_mask = strcmp(header_list, 'user_decision');
user_decision_col = find(exact_match_mask);




%all_spots = {};         % Tracks All Spot(s) selected or not 
defect_spots = {};      % Tracks only defect spot selected or not
ok_spots = {}; 
defect_cnt = 1;
ok_cnt = 1;
%obvi_bad_spots = {}; 
%obvi_bad_cnt = 1;

[spot_cnt, orig_data_width] = size(spot_data);

for ii = 2:spot_cnt
    %all_spots{ii, 1} = ii;                      % Row of spot from master_spot_cell 
    %all_spots{ii, 2} = 1;                       % Use to track usage. '1' is free to use, '0' is already selected 
    
    spot_defect_val = gamefile_cell{ii,defect_col};
    if( (str2num(spot_defect_val) < defect_thresh_hi) & (str2num(spot_defect_val) > defect_thresh_low) )
        defect_spots{defect_cnt,1} = ii;
        defect_spots{defect_cnt, 2} = 1;        % Use to track usage. '1' is free to use, '0' is already selected 
        defect_cnt = defect_cnt + 1; 
        
    %elseif( str2num(spot_defect_val) <= defect_thresh_low )
    else
        ok_spots{ok_cnt,1} = ii;
        ok_spots{ok_cnt, 2} = 1;        % Use to track usage. '1' is free to use, '0' is already selected 
        ok_cnt = ok_cnt + 1;
    
    %{
    elseif( str2num(spot_defect_val) > defect_thresh_hi )
        obvi_bad_spots{obvi_bad_cnt,1} = ii;
        obvi_bad_spots{obvi_bad_cnt, 2} = 1;        % Use to track usage. '1' is free to use, '0' is already selected 
        obvi_bad_cnt = obvi_bad_cnt + 1;
    %}
    end
end 

%===== Choose num_benchmark spots from defect_spots ======%
ORDER_TRACKER = 1; 
last_defect_ind = 1;
for ii = 1:num_benchmark
    defect_spot_ind = defect_spots{last_defect_ind,1};
    defect_spots{last_defect_ind, 2} = 0; 
    %all_spots{last_defect_ind, 2} = 0; 
    last_defect_ind = last_defect_ind + 1; 
    
    %===== Add benchmark spots as extra rows to spot_data =====%
    for jj = 1:user_cnt
        tmp_gamefile_cell = user_gamefiles{jj,1};
        
        tmp_gamefile_cell{defect_spot_ind, annotate_type_col} = 'B';
        tmp_gamefile_cell{defect_spot_ind, select_order_col} = ORDER_TRACKER;
        ORDER_TRACKER = ORDER_TRACKER + 1; 
        
        % Copy spot twice to gamefiles, increment ORDER_TRACKER 
        for kk = 1:benchmark_repeat
            tmp_gamefile_cell(end + 1, :) = gamefile_cell(defect_spot_ind, :);

            % Update the annotate_type 
            tmp_gamefile_cell{end, annotate_type_col} = 'B'; 

            % Update selection_order 
            tmp_gamefile_cell{end, select_order_col} = ORDER_TRACKER;
            ORDER_TRACKER = ORDER_TRACKER + 1;
        end 
        
        ORDER_TRACKER = ORDER_TRACKER - benchmark_repeat - 1;
        
        % Save files back to orig
        user_gamefiles{jj,1} = tmp_gamefile_cell; 
    end
    
    ORDER_TRACKER = ii * (benchmark_repeat + 1) + 1; 
end 


%===== Choose Defect Democracy Spots =====%
% NOTE: Exhaust the defect_spots list of bad spots that fit desired hi and
% low thresholds first 
for ii = 1:num_democracy_defect
    avail_defect_list = find(cell2mat(defect_spots(:,2)));
    
    if(length(avail_defect_list) > 0)
        avail_ind = 1; %randi(length(avail_defect_list));
        defect_spot_ind = defect_spots{avail_defect_list(avail_ind),1};
        %defect_spot_ind = defect_spots{avail_defect_list(1),1};
        defect_spots{avail_defect_list(avail_ind), 2} = 0; 
        %%all_spots{avail_defect_ind(1), 2} = 0; 

        %===== Update spot_data =====%
        for jj = 1:user_cnt
            tmp_gamefile_cell = user_gamefiles{jj,1};
            %tmp_gamefile_cell(end + 1, :) = gamefile_cell(defect_spot_ind, :);

            % Update the annotate_type 
            tmp_gamefile_cell{defect_spot_ind, annotate_type_col} = 'D'; 

            % Update selection_order 
            tmp_gamefile_cell{defect_spot_ind, select_order_col} = ORDER_TRACKER;

            % Save files back to orig
            user_gamefiles{jj,1} = tmp_gamefile_cell; 
        end
        ORDER_TRACKER = ORDER_TRACKER + 1;
    else
        avail_ok_list = find(cell2mat(ok_spots(:,2)));
        avail_ind = 1; %randi(length(avail_ok_list));
        ok_spot_ind = ok_spots{avail_ok_list(avail_ind), 1};
        ok_spots{avail_ok_list(avail_ind), 2} = 0; 
        
        %===== Update spot_data =====%
        for jj = 1:user_cnt
            tmp_gamefile_cell = user_gamefiles{jj,1};
            %tmp_gamefile_cell(end + 1, :) = gamefile_cell(defect_spot_ind, :);

            % Update the annotate_type 
            tmp_gamefile_cell{ok_spot_ind, annotate_type_col} = 'D'; 

            % Update selection_order 
            tmp_gamefile_cell{ok_spot_ind, select_order_col} = ORDER_TRACKER;

            % Save files back to orig
            user_gamefiles{jj,1} = tmp_gamefile_cell; 
            
        end 
        ORDER_TRACKER = ORDER_TRACKER + 1; 
        
    end
end 
%}


%===== Choose other Democracy Spots =====%
% NOTE: ALL USERS GET THE SAME DEMOCRACY SPOTS =====%
for ii = 1:(num_democracy - num_democracy_defect) 
    % Grab a non-defect spot
    avail_ok_list = find(cell2mat(ok_spots(:,2)));
    avail_ind = 1; %randi(length(avail_ok_list));
    ok_spot_ind = ok_spots{avail_ok_list(avail_ind), 1};
    %ok_spot_ind = ok_spots{avail_ok_list(1), 1};
    ok_spots{avail_ok_list(1), 2} = 0; 

    %===== Update spot_data =====%
    for jj = 1:user_cnt
        tmp_gamefile_cell = user_gamefiles{jj,1};
        %tmp_gamefile_cell(end + 1, :) = gamefile_cell(defect_spot_ind, :);

        % Update the annotate_type 
        tmp_gamefile_cell{ok_spot_ind, annotate_type_col} = 'D'; 

        % Update selection_order 
        tmp_gamefile_cell{ok_spot_ind, select_order_col} = ORDER_TRACKER;

        % Save files back to orig
        user_gamefiles{jj,1} = tmp_gamefile_cell;  
    end
    
    ORDER_TRACKER = ORDER_TRACKER + 1; 
end
%}

%test_gamefile = user_gamefiles{1,1};
%order_list = sort(cell2mat(test_gamefile(2:end,32)));

%===== Choose random Coverage spots =====%
% NOTE: Exhaust the defect_spots list of bad spots that fit desired hi and
% low thresholds first
for ii = 1:user_cnt
    ORDER_TRACKER_NEW = ORDER_TRACKER;
    tmp_gamefile_cell = user_gamefiles{ii,1};
    for jj = 1:num_coverage_defect
        % Grab a defect spot
        avail_defect_list = find(cell2mat(defect_spots(:,2)));
        if(length(avail_defect_list) > 0)
            avail_ind = 1;%randi(length(avail_defect_list));
            defect_spot_ind = defect_spots{avail_defect_list(avail_ind),1};
            %defect_spot_ind = defect_spots{avail_defect_ind(1),1};
            defect_spots{avail_defect_list(avail_ind), 2} = 0; 
            %%all_spots{defect_spot_ind, 2} = 0;
            
            % Update the annotate_type 
            tmp_gamefile_cell{defect_spot_ind, annotate_type_col} = 'C'; 

            % Update selection_order 
            tmp_gamefile_cell{defect_spot_ind, select_order_col} = ORDER_TRACKER_NEW;
        else
            avail_ok_list = find(cell2mat(ok_spots(:,2)));
            avail_ind = 1; %randi(length(avail_ok_list));
            ok_spot_ind = ok_spots{avail_ok_list(avail_ind), 1};
            ok_spots{avail_ok_list(avail_ind), 2} = 0;
            
            % Update the annotate_type 
            tmp_gamefile_cell{ok_spot_ind, annotate_type_col} = 'C'; 

            % Update selection_order 
            tmp_gamefile_cell{ok_spot_ind, select_order_col} = ORDER_TRACKER_NEW;
        end 
        ORDER_TRACKER_NEW = ORDER_TRACKER_NEW + 1; 
    end
    % Save files back to orig
    tmp_gamefile_cell{1,1} = 'Dummy'; % cell{1,1} cannot be blank
    user_gamefiles{ii, 1} = tmp_gamefile_cell;
    
    %ORDER_TRACKER = ORDER_TRACKER - (num_coverage_defect);  
end 
%}

%test_gamefile = user_gamefiles{1,1};
%order_list = sort(cell2mat(test_gamefile(2:end,32)));

ORDER_TRACKER = ORDER_TRACKER + (num_coverage_defect);


%===== Randomly Choose OK Coverage spots =====%
for ii = 1:user_cnt
    ORDER_TRACKER_NEW = ORDER_TRACKER;
    tmp_gamefile_cell = user_gamefiles{ii,1};
    for jj = 1:(num_coverage - num_coverage_defect)
        avail_ok_list = find(cell2mat(ok_spots(:,2)));
        avail_ind = 1; %randi(length(avail_ok_list));
        ok_spot_ind = ok_spots{avail_ok_list(avail_ind), 1};
        ok_spots{avail_ok_list(avail_ind), 2} = 0;
        %%all_spots{ok_spot_ind, 2} = 0; 
        
        % Update the annotate_type 
        tmp_gamefile_cell{ok_spot_ind, annotate_type_col} = 'C'; 

        % Update selection_order 
        tmp_gamefile_cell{ok_spot_ind, select_order_col} = ORDER_TRACKER_NEW;
        ORDER_TRACKER_NEW = ORDER_TRACKER_NEW + 1; 
    end
    % Save files back to orig
    user_gamefiles{ii,1} = tmp_gamefile_cell;
    
    %ORDER_TRACKER = ORDER_TRACKER - (num_coverage - num_coverage_defect);  
end 
%}

%===== Analyze Locations of Coverage, Democracy, and Batch Spots =====%
user_bench = cell(user_cnt, 1); 
user_dem = cell(user_cnt, 1);
user_cov = cell(user_cnt, 1); 

for ii = 1:user_cnt
    tmp_gamefile = user_gamefiles{ii,1}; 
    
    work_cell_arr = cell2mat(tmp_gamefile(2:end, annotate_type_col));
    bench_indices = find(work_cell_arr == 'B');
    dem_indices = find(work_cell_arr == 'D'); 
    cov_indices = find(work_cell_arr == 'C');
    
    user_bench{ii, 1} = bench_indices; 
    user_dem{ii, 1} = dem_indices;
    user_cov{ii, 1} = cov_indices;
end 

fprintf('*** RANDOMIZING SPOT ORDER *****\n');
%===== Randomize user_gamefile =====%
for ii = 1:user_cnt
    tmp_gamefile = user_gamefiles{ii,1}; 
    
    work_cell_arr = tmp_gamefile(2:end, select_order_col);
    %empty_index = cellfun('isempty', work_cell_arr);     % Find indices of empty cells
    %work_cell_arr(empty_index) = {0};   

    select_indices = find(cell2mat(work_cell_arr));
    new_order = randperm(num_spots); 
    for xx = 1:length(new_order)
        tmp_gamefile{select_indices(xx) + 1, select_order_col} = new_order(xx); 
    end 
    
    %[~, ~, v2] = find(cell2mat(tmp_gamefile(2:end, select_order_col)));
    
    user_gamefiles{ii,1} = tmp_gamefile; 
end 
fprintf('*** SPOT ORDER RANDOMIZATION COMPLETE *****\n');

%{
%===== TESTING PURPOSES ONLY FOR CHECKING SELECT ORDER =====%
test_gamefile = user_gamefiles{1,1};
order_list = sort(cell2mat(test_gamefile(2:end,32)));
max_spot_cnt = max(order_list);
fprintf(['max_spot_cnt = ' num2str(max_spot_cnt) '\n']);

test_gamefile2 = user_gamefiles{10,1};
order_list2 = sort(cell2mat(test_gamefile2(2:end,32)));
max_spot_cnt2 = max(order_list2);
fprintf(['max_spot_cnt2 = ' num2str(max_spot_cnt2) '\n']);
%}


%===== Convert User Gamefile cells to CSV =====%
fprintf('***** BEGIN CSV GAMEFILE GENERATION *****\n'); 
for ii = 1:user_cnt
    gamefile_cell = user_gamefiles{ii,1};
    user_name = master_user_list{ii,1};
    file_str = fullfile(gamefile_dst_dir, [user_name '_Spots.csv']); 
    fprintf(['*** GENERATING GAMEFILE: ' user_name '\n']);
    cell2csv(file_str, gamefile_cell);
    fprintf(['*** ' user_name ' GAMEFILE COMPLETE \n']);
end 
fprintf('***** CSV GAMEFILE GENERATION COMPLETE *****\n'); 

%===== Generate .json file for ingestion by Spot ID Game =====%
json_dir = fullfile('S:\SpotGameCfg\config.json'); 
%JSONFILE_name = sprintf(json_dir); 
fid = fopen(json_dir,'w'); 
%s = struct( "audioData", targets{y}, "DistractorData", distractors{y}); 
s = struct( 'spot_type', defect_type, ...
            'annotate_type', defect_annotate, ...
            'annotate_label', defect_ui_label); 
encodedJSON = jsonencode(s); 
fprintf(fid, encodedJSON); 
fclose(fid);     
