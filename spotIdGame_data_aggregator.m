%{
PURPOSE: Aggregate N player .csv files from a cycle of the Spot ID Game.
The data is aggregated into multiple products for later algorithm
development use.

PRODUCTS:
    1. 'Master' .csv file:  Contains a final aggregation of all user spot 
                            data
    2. Benchmark Report(s): Analyze player consistency based on benchmark
                            spot performance. Data stored in .txt files 

METHODOLOGY:
    Constructing the Master file begins with a copy of any given .csv
    gamefile; Other than user decisions, presentation order, and assigned
    random 'Coverage' spots, the contents of all gamefiles are identical.

    Because Democracy and Benchmark spots are shared between all users, we
    first identify them to create the democracy_spot and benchmark_spot
    cell arrays. 
    
    Coverage spots are updated with each new user, as there *should* be no
    overlap between user coverage spots: All are unique. 

    'Democracy' Spots are temporarily stored in the cell array 
    democracy_spot. Once all user democracy spot decisions have been
    aggregated, a final decision is made democractically as to the spot's
    status as good or bad.

    'Benchmark' Spots are treated the same way as 'Democracy' Spots. 
%}

close all;
clear all;
clc;

%===== GLOBAL SETTINGS =====%
% Note on BENCHMARK_REPEAT: The last few entries of any gamefile are
% actually repeats of certain selected "benchmark" spots, which are used to
% gauge user consistency, and whether their annotations are valid enough to
% include their data in the current aggregation. BENCHMARK_REPEAT is the
% number of 
BENCHMARK_PASS_SCORE = 0.6;
SPOT_PASS_SCORE = 1.0; 
BENCHMARK_REPEAT_CNT = 10;  %EX: 5 benchmark spots, 2 repeats each at EOF, results in a value of 10 
BENCH_SPOT_REPEAT = 3;  % Number of times a B spot is presented to user
DEFECT_ANNOTATE = 'percentDebris';
BAD_CHIP_THRESH = 0.3;
ROC_THRESHOLD = 0.5; 
LO_THRESH = 0.05; 
HI_THRESH = 0.3;

dir_gamefiles = 'S:\SpotGameCfg\Game Records\week_2_22-Feb-2022';
gamefile_dir = dir(dir_gamefiles);
gamefile_list = {gamefile_dir.name};
% NameList(ismember(NameList, {'.', '..'})) = [];  % Remove . and ..  Not required here
has_csv   = contains(gamefile_list, '.csv');
gamefile_list = gamefile_list(has_csv);
gamefile_list = gamefile_list'; 
[user_cnt,~] = size(gamefile_list); 

%----- Check that there is at least one (1) gamefile in directory -----%
if isempty(gamefile_dir)
    msg = 'There are no Spot Game gamefiles in the directory';
        errordlg(msg,'Data Aggregation Error');
        return
end 

%----- Create a 'Master' csv file -----%
%{
NOTE: Copy the first gamefile data as a cell, and clear its user_decision,
annotation_type, and order columns 
%}
img_col = -1;
decision_col = -1;
annotate_col = -1; 

src_dir_obj = gamefile_dir(1);
master_gamefile_dir = fullfile(src_dir_obj.folder, 'analysis', ['MASTER_GAMEFILE_' date() '.csv']);
src_gamefile_dir = fullfile(src_dir_obj.folder, gamefile_list{1,1});
[~, ~, master_gamefile] = xlsread(src_gamefile_dir);
master_table = cell2table(master_gamefile(2:end, :), ...
                                     'VariableNames',master_gamefile(1, :));

%--- Reset select_order, user_decision, and annotate_types ('C' Only) ---%
spot_cnt = height(master_table); 

%annotate_data_type = class(master_table.annotate_type(1));
for ii = 1:spot_cnt
    
    %--- Reset select_order ---%
    master_table.select_order(ii) = 0; 
    
    %--- Reset user_decision ---%
    master_table.user_decision(ii) = -1; 
    
    %--- Reset Annotate Types ('Coverage' only)---%
    % NOTE: 'D' and 'B' are the same for all users for any given game
    % cycle. There is no need to reset them
    dec_value = master_table.annotate_type(ii);
    if strcmp(dec_value{1,1}, 'C')
        master_table.annotate_type(ii) = {'X'}; 
    end 
end 

%--- Read all user gamefiles ---%
user_report_cell = {'name' 'spots_total' 'spots_completed' 'benchmark_result'};
total_democracy_spots = {};
total_benchmark_spots = {}; 
for ii = 1:user_cnt
    spots_completed = 0;
    spots_total = 0; 
    user_gamefile_dir = fullfile(src_dir_obj.folder, gamefile_list{ii,1});
    fprintf([gamefile_list{ii,1} '\n']); 
    [~, ~, raw] = xlsread(user_gamefile_dir); 
    user_table = cell2table(raw(2:end, :), ...
                                     'VariableNames',raw(1, :));
    
    %Assess user benchmark results. If user's score is lower than 
    %BENCHMARK_PASS_SCORE, his/her annotations are not aggregated into the
    %master .csv gamefile 
    [benchmark_spot_score, benchmark_spots, benchmark_score] = assessBenchmark(user_table, BENCH_SPOT_REPEAT, SPOT_PASS_SCORE);
    [benchmark_spot_cnt, ~] = size(benchmark_spots); 
    
    fprintf([gamefile_list{ii,1} ' BENCHMARK SCORE: ' num2str(benchmark_score) '\n']); 
    
    
    %----- User data is valid for aggregation -----%
    
    % NOTE: We don't want to include the repeat Benchmark spots at the
    % end of each gamefile
    for jj = 1:(spot_cnt - BENCHMARK_REPEAT_CNT)
        spot_name = user_table.img_path(jj);
        spot_dec = user_table.user_decision(jj);
        dec_value = user_table.annotate_type(jj);

        if strcmp(dec_value{1,1}, 'C') | strcmp(dec_value{1,1}, 'D') | ...
                strcmp(dec_value{1,1}, 'B')
            spots_total = spots_total + 1; 
        end 
        
        if strcmp(dec_value{1,1}, 'C') & spot_dec ~= -1
            spots_completed =spots_completed + 1; 
            
            if benchmark_score >= BENCHMARK_PASS_SCORE
                master_table.user_decision(jj) = user_table.user_decision(jj); 
                master_table.annotate_type(jj) = {'C'}; 
            end 

        % Aggregate 'Democracy' Spot data. Make a final decision AFTER
        % all user gamefiles have been visited 
        elseif strcmp(dec_value{1,1}, 'D') & spot_dec ~= -1
            spots_completed =spots_completed + 1;

            if benchmark_score >= BENCHMARK_PASS_SCORE
                if isempty(total_democracy_spots)
                    total_democracy_spots{end+1,1} = spot_name{1,1};
                    total_democracy_spots{end, 2} = [spot_dec];
                else
                    match_mask = strcmp(total_democracy_spots(:,1), spot_name{1,1});
                    if match_mask == 0
                        total_democracy_spots{end+1,1} = spot_name{1,1};
                        total_democracy_spots{end, 2} = [spot_dec]; 
                    else
                        democracy_row = find(match_mask);
                        old_arr = total_democracy_spots{democracy_row, 2};
                        total_democracy_spots{democracy_row, 2} = [old_arr ; spot_dec]; 
                    end 
                end
            end 
            %master_table.user_decision(ii) = user_table.user_decision(ii); 

        % Aggregate 'Democracy' Spot data. Make a final decision AFTER
        % all user gamefiles have been visited 
        elseif strcmp(dec_value{1,1}, 'B') & spot_dec ~= -1
            spots_completed =spots_completed + 1;
            
            if benchmark_score >= BENCHMARK_PASS_SCORE
                if isempty(total_benchmark_spots)
                    total_benchmark_spots{end+1,1} = spot_name{1,1};
                    total_benchmark_spots{end, 2} = [spot_dec];
                else
                    match_mask = strcmp(total_benchmark_spots(:,1), spot_name{1,1});
                    if match_mask == 0
                        total_benchmark_spots{end+1,1} = spot_name{1,1};
                        total_benchmark_spots{end, 2} = [spot_dec]; 
                    else
                        benchmark_row = find(match_mask);
                        old_arr = total_benchmark_spots{benchmark_row, 2};
                        total_benchmark_spots{benchmark_row, 2} = [old_arr ; spot_dec]; 
                    end 
                end 
            end 
            %master_table.user_decision(ii) = user_table.user_decision(ii); 
        end 
    end
    
    user_report_cell{ii + 1, 1} = gamefile_list{ii,1};
    user_report_cell{ii + 1, 2} = spots_total;
    user_report_cell{ii + 1, 3} = spots_completed;
    user_report_cell{ii + 1, 4} = benchmark_score;
    
    % Add individual benchmark spot columns, record user choices for each
    for xx = 1:benchmark_spot_cnt
        benchmark_str_arr = num2str(benchmark_spots{xx, 2}); 
        user_report_cell{1, 4 + xx} = ['SPOT_' num2str(xx)]; 
        user_report_cell{ii + 1, 4 + xx} = num2str(benchmark_spots{xx, 2}'); 
    end 
end

%----- Remove repeat benchmark spot rows at end of master_gamefile -----%
spot_cnt_orig = spot_cnt - BENCHMARK_REPEAT_CNT; 
master_table = master_table(1:(end - BENCHMARK_REPEAT_CNT), :); 

%----- Decide final annotation for Democracy and Benchmark spots -----%
dem_decisions = spotDecisionDemocracy(total_democracy_spots);
bench_decisions = spotDecisionDemocracy(total_benchmark_spots); 

[dem_cnt, ~] = size(dem_decisions);
[bench_cnt, ~] = size(bench_decisions); 

for ii = 1:dem_cnt
    match_mask = strcmp(master_table.img_path, dem_decisions{ii,1});
    dem_row = find(match_mask);
    if dem_row ~= 0
        master_table.user_decision(dem_row) = dem_decisions{ii,2};
    end 
end 

for ii = 1:bench_cnt
    match_mask = strcmp(master_table.img_path, bench_decisions{ii,1});
    bench_row = find(match_mask);
    if bench_row ~= 0
        master_table.user_decision(bench_row) = bench_decisions{ii,2}; 
    end 
end 

%----- Collect all annotated spot data for ROC Curve -----%
%{
raw_defect_arr = master_table.(DEFECT_ANNOTATE);
anno_defect_arr = master_table.user_decision;
anno_type_arr = master_table.annotate_type;
roc_data = {};
roc_cnt = 1;
for ii = 1:length(raw_defect_arr)
    if anno_defect_arr(ii) ~= -1
        roc_data{roc_cnt, 1} = raw_defect_arr(ii);
        if raw_defect_arr(ii) >= BAD_CHIP_THRESH
            roc_data{roc_cnt,2} = 0;
        else
            roc_data{roc_cnt,2} = 1;
        end 
        roc_data{roc_cnt,3} = anno_defect_arr(ii);
        roc_cnt = roc_cnt + 1; 
    end 
end

roc_cnt = roc_cnt - 1; 
roc_data = cell2mat(roc_data); 


%----- Calculate ROC Curve -----%
% Binary Response Variable
%resp = (1:roc_cnt)' > 50; 

% Fit a logistic regression model
%mdl = fitglm(roc_data(:,3),resp,'Distribution','binomial','Link','logit');

% Compute ROC curve. Use probability estimates from log. regr model as
% scores 
%scores = mdl.Fitted.Probability;


[X,Y,T,AUC] = perfcurve(roc_data(:,3),roc_data(:,1), 0);

figure;
hold on;
plot(X,Y);
xlabel('False positive rate'); 
ylabel('True positive rate');
title(['ROC Curve (T = ' num2str(BAD_CHIP_THRESH) ')']);
plot([0 1],[0 1], 'r-');  
hold off; 
%}

%[FPR, TPR] = perfcurve(roc_data(:,3), roc_data(:,2), 1); 
%figure;
%plot(FPR, TPR, 'lineWidth', 3); 

%----- Save User Performance Table as CSV -----%
user_report_dir = fullfile(src_dir_obj.folder, 'analysis', ['USER_REPORT_' date() '.csv']);
user_report_table = cell2table(user_report_cell(2:end, :), ...
                                     'VariableNames',user_report_cell(1, :));
writetable(user_report_table, user_report_dir); 
%}

%----- Save 'Master_Gamefile' -----%
writetable(master_table, master_gamefile_dir);

fprintf('***** SPOT DATA AGGREGATION COMPLETE *****\n');


%----- Benchmark Assessment Function -----%
% Assess whether a given player's gamefile is usable based on benchmark
% spot annotation consistency 
function [benchmark_spot_scores, benchmark_spots, benchmark_score] = assessBenchmark(user_table, repeat, pass)
    %benchmark_score = 0; 
    benchmark_spots = {};
    benchmark_spot_scores = {}; 
    %bench_cnt = 1;

    spot_cnt = height(user_table); 
    %----- Set the variable spot values (user_de
    for ii = 1:spot_cnt
        if strcmp(user_table.annotate_type(ii), {'B'})
            spot_name = user_table.img_path(ii);
            spot_dec = user_table.user_decision(ii);
        
            if isempty(benchmark_spots)
                benchmark_spots{end+1,1} = spot_name{1,1};
                benchmark_spots{end, 2} = [spot_dec];
            else
                match_mask = strcmp(benchmark_spots(:,1), spot_name{1,1});
                if match_mask == 0
                    benchmark_spots{end+1,1} = spot_name{1,1};
                    benchmark_spots{end, 2} = [spot_dec]; 
                else
                    benchmark_row = find(match_mask);
                    old_arr = benchmark_spots{benchmark_row, 2};
                    benchmark_spots{benchmark_row, 2} = [old_arr ; spot_dec]; 
                end 
            end 
        end 
        %}
    end
    
    [total_spots,~] = size(benchmark_spots);
    aggregate_score = 0; 
    for ii = 1:total_spots
        dec_arr = benchmark_spots{ii,2};
        [~, F] = mode(dec_arr); 
        % Check if all decisions are the same. If not, consistency is not
        % present. This benchmark spot "fails"
        benchmark_spot_scores{end + 1, 1} = F/repeat; 
        if (F/repeat) >= pass
             aggregate_score = aggregate_score + 1; 
        end 
    end 
    
    benchmark_score = aggregate_score / total_spots; 
    
    %{
    if benchmark_score >= pass_score
        benchmark_score = 1; 
    else
        benchmark_score = 0;
    end
    %}
end 

function decision = spotDecisionDemocracy(spot_cell)
    decision = spot_cell;
    
    [spot_cnt, col] = size(decision);
    
    for ii = 1:spot_cnt
        dec_arr = decision{ii,2}; 
        mode_val = mode(dec_arr);
        decision{ii,2} = mode_val; 
    end 
end 
%{

%----- Read all player gamefiles, fill in master_
[~, ~, master_gamefile] = xlsread(master_gamefile_dir);
[m, ~] = size(master_gamefile);
spot_cnt = m - 1; 
master_gamefile_headers = master_gamefile(1,:); 

% Find img_path column
exact_match_mask = strcmp(master_gamefile_headers, 'img_path');
img_col = find(exact_match_mask);
    
% Find user_decision column 
exact_match_mask = strcmp(master_gamefile_headers, 'user_decision');
decision_col = find(exact_match_mask);

% Find user_decision column 
exact_match_mask = strcmp(master_gamefile_headers, 'annotate_type');
annotate_col = find(exact_match_mask);



democracy_spots = {};
benchmark_spots = {};
dem_cnt = 1; 
bench_cnt = 1;

%----- Set the variable spot values (user_de
for ii = 1:spot_cnt
    spot_row = master_gamefile(ii + 1, :);
    spot_name = spot_row{1, img_col};
    spot_dec = spot_row{1, decision_col}; 
    spot_type = spot_row{1,annotate_col};

    %{
    if strcmp(spot_type, 'D')
        democracy_spots{dem_cnt,1} = ii + 1; 
        democracy_spots{dem_cnt,2} = [spot_dec]; 
        dem_cnt = dem_cnt + 1; 
        
    elseif strcmp(spot_type, 'B')
        if isempty(benchmark_spots)
            benchmark_spots{end+1,1} = spot_name;
            benchmark_spots{end, 2} = [spot_dec];
        else
            match_mask = strcmp(benchmark_spots(:,1), spot_name);
            if match_mask == 0
                benchmark_spots{end+1,1} = spot_name;
                benchmark_spots{end, 2} = [spot_dec]; 
            else
                benchmark_row = find(match_mask);
                old_arr = benchmark_spots{benchmark_row, 2};
                benchmark_spots{benchmark_row, 2} = [old_arr ; spot_dec]; 
            end 
        end 
    end
    %}
end 



benchmark_report = {master_dir_obj.name benchmark_spots}; 
%----- Create cell array of democracy spots -----%
%NOTE: each cell array row contains the spot identification, and an empty
%      1x0 array. As wel= loop through user gamefiles, we'll fill in user
%      decisions into this array like a stack. We'll then democratically
%      decide the final status of each democracy spot by majority vote



for ii = 1:length(gamefile_dir)
    gamefile_dir_obj = gamefile_dir(ii);
    user_gamefile_dir = [gamefile_dir_obj.folder '\' gamefile_dir_obj.name];

    [~, ~, raw] = xlsread(handles.user_gamefile_dir); 
    handles.user_gamefile = raw;
    [m, ~] = size(handles.user_gamefile); 
    handles.gamefile_total_spot_cnt = m - 1;
    handles.user_gamefile_headers = raw(1,:);

    % Calculate gamefile_spot_cnt i.e. spots selected for this cycle
    handles.select_order_arr = cell2mat(handles.user_gamefile(2:end, handles.order_col));
    handles.gamefile_spot_cnt = length(find(handles.select_order_arr));

    % Check if all spots are annotated or not. If so, don't even proceed to
    % the game panel 
    if(handles.start_order > handles.gamefile_spot_cnt)
        msgbox('You have already completed the current set of spot annotations.', 'CONGRATULATIONS!');
        return
    end

end
%}