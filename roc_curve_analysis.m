%{
AUTHOR: Richard Cnen
DATE: 3 Mar 2022
PURPOSE: Analyze aggregated spot annotation data in the following ways:
            1. Generate ROC curve comparing annotated results to values
            predicted by existing NanoQC system 
%}

close all;
clear all;
clc;

%----- Parameters -----%
DEFECT_ANNOTATE = 'similarity';
master_gamefile_dir = 'S:\SpotGameCfg\Game Records\week_2_22-Feb-2022\analysis\MASTER_GAMEFILE_22-Feb-2022.csv'; 

[~, ~, master_gamefile] = xlsread(master_gamefile_dir);
master_table = cell2table(master_gamefile(2:end, :), ...
                                     'VariableNames',master_gamefile(1, :));


%----- Collect all annotated spot data for ROC Curve -----%
% roc_data(:, 1): Original DEFECT_ANNOTATE value
% roc_data(:, 2): Player annotated spot decisions ('Actual' result)
raw_defect_arr = master_table.(DEFECT_ANNOTATE);
anno_defect_arr = master_table.user_decision;
anno_type_arr = master_table.annotate_type;
roc_data = {};

% IGNORE UN-ANNOTATED SPOTS (-1) AND FAIL-OTHER SPOTS (2) 
for ii = 1:length(raw_defect_arr)
    if anno_defect_arr(ii) ~= -1 | anno_defect_arr(ii) == 2
        if(iscell(raw_defect_arr(ii)))
            tmp_cell = raw_defect_arr(ii)
            tmp_defect = tmp_cell{1,1}; 
            if isnumeric(tmp_defect)
                roc_data{end + 1, 1} = tmp_defect;
            else
                roc_data{end + 1, 1} = 0.0;
            end 
        else
            if isnumeric(raw_defect_arr(ii))
                roc_data{end + 1, 1} = raw_defect_arr(ii);
            else
                roc_data{end + 1, 1} = 0.0;
            end 
        end 
        roc_data{end,2} = anno_defect_arr(ii);
    end 
end

roc_data = cell2mat(roc_data);
P = length(find(roc_data(:,2) == 1)); 
N = length(find(roc_data(:,2) == 0)); 
[roc_cnt, ~] = size(roc_data); 

% Normalize raw_defect_arr value
normal_data = roc_data(:,1); 
normal_data = (normal_data - min(normal_data)) / ( max(normal_data) - min(normal_data) );
roc_data(:,1) = normal_data; 



%----- Generate Histogram of Data -----%
% Split Data into good/bad spots
bad_spots = [];
good_spots = [];
for ii = 1:roc_cnt
    if(roc_data(ii,2) == 1)
        bad_spots = [bad_spots; roc_data(ii,1)]; 
    else
        good_spots = [good_spots; roc_data(ii,1)]; 
    end
end 

fig1 = figure;
hold on;
histogram(good_spots, 'EdgeColor', 'b');
histogram(bad_spots, 'EdgeColor', 'r');
title(['Annotated Spot Histogram (' DEFECT_ANNOTATE ')']); 
xlabel([DEFECT_ANNOTATE ' Value']);
%xlim([0 2]); 
ylabel('Count'); 
hold off; 

sens_spec_table = zeros(100, 3);
for ii = 1:100
    thresh = ii / 100;
    sens_spec_table(ii,1) = thresh; 
    TN = 0;
    TP = 0; 
    for jj = 1:roc_cnt
        percent_deb_val = roc_data(jj, 1);
        roc_anno = roc_data(jj,2); 
        debris_dec = percent_deb_val > thresh;
        
        % Aggregate True Pos
        if roc_anno == 1
            if debris_dec == roc_anno
                TP = TP + 1;
            end
        
        % Aggregate True Neg
        else
            if debris_dec == roc_anno
                TN = TN + 1;
            end 
        end
        
    end
    
    fprintf('TPR = %d       TNR = %d\n', (TP/P), (TN / N)); 
    sens_spec_table(ii, 2) = TP / P;
    sens_spec_table(ii, 3) = 1 - (TN / N); 
end 

%----- Plot ROC Curve -----%
fig2 = figure;
hold on;
plot(sens_spec_table(:, 3), sens_spec_table(:, 2), 'b');
xlabel('False positive rate'); 
ylabel('True positive rate');
title(['ROC Curve (' DEFECT_ANNOTATE ')']);
plot([0 1],[0 1], 'r-');  
hold off; 